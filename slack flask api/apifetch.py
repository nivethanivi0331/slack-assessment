# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 19:08:02 2021

@author: saravanan
"""


from pymongo import MongoClient

client = MongoClient("mongodb+srv://queen:daddylittleprincess@cluster0.tcznb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
Database = client['Maroon_Insights_Driver']
collection = Database['Driver_Results']

import json
import requests
import sys
from flask import Flask,render_template,request,redirect


app = Flask(__name__)

@app.route('/', methods = ["POST","GET"])


def apifetch():
    if request.method =="POST":
        return redirect(request.form["Name"])

    return render_template('index1.html')

@app.route("/<user_name>/")


def slack_messages(user_name):
    try:
        send_to_list = collection.find({"Name": 1,"Email": 0,"Message": 1})
        for item in send_to_list:
             userName = item['Name']
             message= item['Message']
             slack_messages(userName,message)
             def slack_messages(user_name,Message):
                    slack_data = {
                        "username" : user_name,
                        "icon_emoji" : ":company:",
                        "channel" : "#maroon",
                        "text" : Message,
                        "attachments": [
                        {
                            "fields": [
                                {
                                    "Name": user_name,
                                    "Email": "false",
                                    "Message": Message,
                                }
                            ]
                        }
                
                        ]
                    }
                    byte_length = str(sys.getsizeof(slack_data))
                    headers = {'content_type':"application/json",'content_length':byte_length}
                    response = requests.post('https://hooks.slack.com/services/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',data=json.dumps(slack_data),headers=headers)
                    print (response.txt)
    except:
        pass
    
    return f'Message sent to {user_name}'

if __name__ == "__main__":
    app.run(debug = False)